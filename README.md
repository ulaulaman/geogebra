# Applet GeoGebra

Raccolta delle *applet* [*GeoGebra*](https://www.geogebra.org), presenti anche sul sito ufficiale del progetto ([profilo](https://www.geogebra.org/u/gianluigi.ulaula))

* [Determinazione dell'altezza del Sole](applet/gnomone.ggb)
* [Quadratura del cerchio per trascinamento](applet/quaddratura_cerchio_trascinamento.ggb)
* [Il triangolo di Napoleone](applet/triangolo_di_napoleone.ggb)
* [Sperimentare con la gravità](applet/gravita.ggb)
* [Il simbolo della pace](applet/simbolo_pace.ggb)

## Rilasci

* [2022.0718](https://gitea.it/ulaulaman/geogebra/releases/tag/2022.0718)